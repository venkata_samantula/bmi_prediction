# -*- coding: utf-8 -*-
"""
Created on Tue Jan 11 16:09:28 2022

@author: T9939VS
"""
import pandas as pd
from sklearn.model_selection import train_test_split
import xgboost as xg
import numpy as np
from sklearn.metrics import mean_squared_error as MSE

path = r"C:\Users\T9939VS\data\data.csv"
df = pd.read_csv(path)

"""
1: Create the training dataset based on the BMI business rules. 
"""

def get_quote(age, bmi, gender):
    """
    Parameters
    ----------
    age : INT
        Age of Person.
    bmi : FLOAT
        Body Mass Index.
    gender : STR
        Gender of Person.

    Returns
    -------
    TYPE
        Quote Price.
    str
        Reason.

    """
    discount = 0.9 if gender == 'Female' else 1
    if (18 <= age <=39 ) and (bmi < 17.49 or bmi > 38.5):
        
        return 750 * discount , "Age is between 18 to 39 and 'BMI' is either less than 17.49 or greater than 38.5"
    elif (40 <= age <= 59 ) and (bmi < 18.49 or bmi > 38.5):
        
        return 1000 * discount , "Age is between 40 to 59 and 'BMI' is either less than 18.49 or greater then 38.5"
    
    elif (age >= 60 ) and (bmi < 18.49 or bmi > 45.5):
        
        return 2000 * discount , "Age is greater than 60 and 'BMI' is either less than 18.49 or greater than 38.5"
    else:
        return 500 * discount, "BMI is in right range"
      
        
df['inches'] = df[['Ht']].astype(str).apply(lambda x: int(x[0][0])*12 + int(x[0][1:]), axis = 1)

"""
BMI : 703 ร— (155(pounds) รท (Height * Height (inches)))
"""
df['bmi'] = df.apply(lambda x : 703 * (x['Wt']/ x['inches']**2) , 
                     axis = 1)

# Get Quote and Reason Code
df[['quote', 'reason']] = df.apply(lambda x :get_quote(x['Ins_Age'], x['bmi'], x['Ins_Gender']), 
                                   axis =1, 
                                   result_type='expand')

#%%

"""
2 : BMI Prediction Model using XGBoost Regression
"""
# Map model into number to pass into model
# No Need of One hot encoding because XGBoost is robust in handling categorical data

df['gender_map'] = df['Ins_Gender'].map({'Male':0, 'Female':1})

train_cols = ['Ins_Age', 'Wt', 'inches',  'gender_map']

X = df[train_cols]
y = df['bmi']

# Splitting the data for train and test sets
train_X, test_X, train_y, test_y = train_test_split(X, y,
                      test_size = 0.3, random_state = 123)
  
# Instantiation
model_bmi = xg.XGBRegressor(objective ='reg:linear',
                  n_estimators = 10, seed = 123)
  
# Fitting the model
model_bmi.fit(train_X, train_y)
  
# Predict the model
pred = model_bmi.predict(test_X)

test_X['pred'] = pred
test_X['actual'] = test_y

# RMSE Computation
rmse = np.sqrt(MSE(test_y, pred))
print("RMSE : % f" %(rmse))


#%%

"""
3: Write steps to operationalize the model.
- We could pickle model for real time predictions
- We could train model in offline with new data.
- We could use AWS Sagemaker pipeline with step function for deployments or We could you AWS lambda or Azure Function for getting 
    real time predictions
"""



